"""
This file was generated with the customdashboard management command and
contains the class for the main dashboard.

To activate your index dashboard add the following to your settings.py::
    GRAPPELLI_INDEX_DASHBOARD = 'pado.dashboard.CustomIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _
from grappelli.dashboard import modules, Dashboard


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for www.
    """

    def init_with_context(self, context):

        self.children.append(modules.ModelList(
            _('Purchase service'),
            column=2,
            collapsible=False,
            models=(
                'app.models.Category',
                'app.models.Product',
                'app.models.Shop',
                'app.models.Dealer',
                'app.models.Catalogue',
            ),
        ))

        self.children.append(modules.AppList(
            _('Exhaustive models list'),
            collapsible=True,
            column=1,
            css_classes=('collapse closed',),
            exclude=('django.contrib.*',),
        ))

        self.children.append(modules.RecentActions(
            _('Recent Actions'),
            limit=5,
            collapsible=False,
            column=3,
        ))



