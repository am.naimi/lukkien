from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel


class Dealer(models.Model):
    name = models.CharField(max_length=50, verbose_name=_('dealer name'))
    products = models.ManyToManyField('Product', blank=True, verbose_name=_('products'))

    def __str__(self):
        return self.name


class Product(models.Model):
    title = models.CharField(max_length=120, null=False, verbose_name=_('Product name'),
                             help_text=_('The product name/title.'))
    description = models.TextField(null=True, blank=True, verbose_name=_('Product description'),
                                   help_text=_('Product description and some details.'))
    product_details_form = models.ForeignKey('fobi.FormEntry', on_delete=models.SET_NULL,
                                             verbose_name=_('Properties form'),
                                             help_text=_(
                                                 'Use the link in the administration panel home page to define new product details form.'),
                                             null=True, blank=True)
    categories = models.ManyToManyField('Category', verbose_name=_('Categories'), help_text=_(
        'Please note that *at least* one of the selected categories should be a leaf node on categories tree. i.e. It should not have any child categories.'))

    def __str__(self):
        return self.title


class Category(MPTTModel):
    name = models.CharField(max_length=50, verbose_name=_('Category name'))
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True,
                            verbose_name=_('Parent category'), on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Shop(models.Model):
    dealer = models.ForeignKey('Dealer', verbose_name=_('dealer'), related_name='shops', on_delete=models.CASCADE)
    shop_name = models.CharField(max_length=100, verbose_name=_('Shop name'))
    address = models.CharField(max_length=100, verbose_name=_('address'))

    def __str__(self):
        return self.shop_name


class Item(models.Model):
    catalogue = models.ForeignKey('Catalogue', verbose_name=_('catalogue'), on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name=_('product'))
    dealer = models.ForeignKey(Dealer, on_delete=models.CASCADE, verbose_name=_('dealer'))

    def __str__(self):
        return self.dealer.name + ' - ' + self.product.title


class Catalogue(models.Model):
    user = models.OneToOneField(User, verbose_name=_('user'), on_delete=models.CASCADE)
    items = models.ManyToManyField(Product, through=Item, verbose_name=_('item'))

    def __str__(self):
        return self.user.username


