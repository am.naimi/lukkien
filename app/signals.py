from django.db.models.signals import post_save
from django.dispatch import receiver

from app.models import Product


@receiver(post_save, sender=Product)
def notify_dealers(sender, created, instance, **kwargs):
    """
    Notify dealers of new product has published

    :param sender:
    :param instance:
    :param kwargs:
    :return:
    """
    if created:
        # TODO email all dealers a new product has been published
        pass