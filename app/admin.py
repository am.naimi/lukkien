from django.contrib import admin
from django import forms
from django_mptt_admin.admin import DjangoMpttAdmin

from app.models import Category, Dealer, Shop, Product, Item, Catalogue


@admin.register(Category)
class CategoryAdmin(DjangoMpttAdmin):
    search_fields = ('name', 'parent__name')


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    search_fields = ('title', )
    filter_horizontal = ('categories', )


class ShopInlineAdmin(admin.TabularInline):
    model = Shop
    extra = 0


class DealerAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_filter = ('name',)
    search_fields = ('name',)
    filter_horizontal = ('products',)
    inlines = [ShopInlineAdmin]


class ShopAdmin(admin.ModelAdmin):
    list_display = ('shop_name', 'address')
    list_filter = ('shop_name', 'address')
    search_fields = ('shop_name', 'address')


class ProdocutInlineAdmin(admin.StackedInline):
    model = Product
    extra = 0

    def get_queryset(self, request):
        return Product.objects.filter(pk=1)


class ItemInlineFormset(forms.models.BaseInlineFormSet):

    def clean(self):
        count = 0
        for form in self.forms:
            try:
                if not form.cleaned_data['product'] in form.cleaned_data['dealer'].products.all():
                    count += 1
            except Exception:
                pass
        if count > 0:
                raise forms.ValidationError('dealer and product are not match')


class ItemAdmin(admin.StackedInline):
    formset = ItemInlineFormset
    model = Item
    extra = 0


class CatalogueAdmin(admin.ModelAdmin):
    list_display = ('user', '_products', '_dealers')
    search_fields = ('user', )
    inlines = [ItemAdmin]

    def _products(self, obj):
        return "\n".join([i.title for i in obj.items.all()])

    def _dealers(self, obj):
        return "\n".join([i.name for i in obj.items.all()])


admin.site.register(Dealer, DealerAdmin)
admin.site.register(Shop, ShopAdmin)
admin.site.register(Catalogue, CatalogueAdmin)
